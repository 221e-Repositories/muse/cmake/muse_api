#ifndef COMMAND_RESPONSE_H
#define COMMAND_RESPONSE_H

#include "Muse_HW.h"

#include <vector>

class CommandResponse {

private:
    /// <summary>Acknowledge response command</summary>
    Muse_HW::Command rx;
    /// <summary>Command on which the acknowledge has been sent</summary>
    Muse_HW::Command tx;

    /// <summary>Payload length</summary>
    int len;
    /// <summary>Acknowledge error code</summary>
    Muse_HW::AcknowledgeType ack;

    /// <summary>Payload content</summary>
    std::vector<uint8_t> payload;

    void setRX(Muse_HW::Command rx);
    void setTX(Muse_HW::Command tx);
    void setLen(int len);
    void setACK(Muse_HW::AcknowledgeType ack);
    void setPayload(const std::vector<uint8_t> &inBuffer, size_t payload_size);

public:
    Muse_HW::Command getRX();
    Muse_HW::Command getTX();
    int getLen();
    Muse_HW::AcknowledgeType getACK();
    std::vector<uint8_t> getPayload();

    CommandResponse();

    /// <summary>CommandResponse constructor</summary>
    /// <param name="buffer">Byte array representation of the received command response</param>
    CommandResponse(const uint8_t* inBuffer, size_t bufferSize);
};

#endif
