#ifndef MUSE_DATA_H
#define MUSE_DATA_H

#include "Range.h"

class Muse_Data {
    private:
        static const int CHANNEL_SIZE_2 = 2;
        static const int CHANNEL_SIZE_3 = 3;
        static const int CHANNEL_SIZE_4 = 4;

        float gyr[CHANNEL_SIZE_3];
        float axl[CHANNEL_SIZE_3];
        float mag[CHANNEL_SIZE_3];
        float hdr[CHANNEL_SIZE_3];
        float th[CHANNEL_SIZE_2];
        float tp[CHANNEL_SIZE_2];

        Range range;
        
        float sound;

        float quat[CHANNEL_SIZE_4];
        float euler[CHANNEL_SIZE_3];

        unsigned long timestamp;
        unsigned long overall_timestamp;

        void setGyroscope(float x, float y, float z);
        void setAccelerometer(float x, float y, float z);
        void setMagnetometer(float x, float y, float z);
        void setHDR(float x, float y, float z);
        void setTH(float temperature, float humidity);
        void setTP(float temperature, float pressure);

        void setRange(Range range);

        void setSound(float sound);

        void setQuaternion(float qw, float qi, float qj, float qk);
        void setEuler(float roll, float pitch, float yaw);

        void setTimestamp(unsigned long timestamp);
        void setOverallTimestamp(unsigned long overall_timestamp);
    
    public:

        Muse_Data();
        Muse_Data(float gyr[], float axl[], float mag[], float hdr[], float th[], float tp[], Range range, float sound, float quat[], unsigned long timestamp, unsigned long overall_timestamp);

        float* getGyroscope();
        float* getAccelerometer();
        float* getMagnetometer();
        float* getHDR();
        float* getTH();
        float* getTP();

        Range getRange();

        float getSound();

        float* getQuaternion();
        float* getEuler();

        unsigned long getTimestamp();
        unsigned long getOverallTimestamp();

};
#endif