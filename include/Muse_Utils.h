#ifndef MUSE_UTILS_H
#define MUSE_UTILS_H

#include "Muse_HW.h"
#include "CommandResponse.h"

#include <stdint.h>
#include <string>
#include <vector>

class Muse_Utils {
    private:
        static const int QUATERNION_DIMENSION = 4;
        static const int EULER_DIMENSION = 3;
        
    public:
        static void GetAnglesFromQuaternion(float q[QUATERNION_DIMENSION], float *outEuler);

        /// <summary>Adds header and trailer to a standard command.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="buffer">Input byte array to be wrapped.</param>
        static void WrapMessage(std::vector<uint8_t> &buffer);

        /// <summary>Removes header and trailer from a standard command.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="buffer">Input byte array from which the header and trailer must be removed.</param>
        static void ExtractMessage(const uint8_t *inBuffer, size_t bufferSize, std::vector<uint8_t> &outBuffer);

        /// <summary>Builds command to retrieve current system state.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_GetSystemState(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to start acquisition in stream mode.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="mode">Identifies the set of data to be acquired.</param>
        /// <param name="frequency">Identifies the data acquisition frequency.</param>
        /// <param name="enableDirect">Allows to select the stream type (i.e., direct or buffered). </param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_StartStream(Muse_HW::DataMode mode, Muse_HW::DataFrequency frequency, std::vector<uint8_t> &outBuffer, bool enableDirect = false, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to start acquisition in log mode.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="mode">Identifies the set of data to be acquired.</param>
        /// <param name="frequency">Identifies the data acquisition frequency.</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_StartLog(Muse_HW::DataMode mode, Muse_HW::DataFrequency frequency, std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to stop any data acquisition procedure.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_StopAcquisition(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);   

        /// <summary>Builds command to set restart device.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="restartMode">Allows to select the restart mode (i.e., reset or boot).</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_Restart(std::vector<uint8_t> &outBuffer, Muse_HW::RestartMode restartMode = Muse_HW::RestartMode::RESET, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);  

        /// <summary>Builds command to retrive application firmware information.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_GetApplicationInfo(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to retrieve battery charge level [%].</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_GetBatteryCharge(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to retrieve battery voltage level [mV].</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_GetBatteryVoltage(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to retrieve system check-up register value.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_GetDeviceCheckup(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to retrieve current firmware versions (i.e., both bootloader and application).</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_GetFirmwareVersion(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to update date/time.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_SetTime(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to retrieve current date/time.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_GetTime(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to retrieve device name (i.e., the name advertised by the Bluetooth module).</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_GetDeviceName(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);
        
        /// <summary>Builds command to change the device name (i.e., the name advertised by the Bluetooth module).</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        // static void Cmd_SetDeviceName(const string &bleName, Muse_HW::CommunicationChannel channel, uint8_t* outBuffer);

        /// <summary>Builds command to retrieve the device unique identifier.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_GetDeviceID(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to retrieve the devices skills (i.e. hardware or software features provided by the device). By default hardware skills are provided.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="softwareSkills">Allows to select the type of features of interest enabling or disabling software skills flag.</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_GetDeviceSkills(std::vector<uint8_t> &outBuffer, bool softwareSkills = false, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to retrieve current memory status.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_GetMemoryStatus(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to erase device memory.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="bulk_erase"></param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_EraseMemory(std::vector<uint8_t> &outBuffer, uint8_t bulk_erase = 0, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to retrieve file information.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="fileId">File identifier.</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_MemoryFileInfo(std::vector<uint8_t> &outBuffer, unsigned short fileId, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to activate a memory file offload procedure.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="fileId">File identifier.</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_MemoryFileDownload(std::vector<uint8_t> &outBuffer, unsigned short fileId, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to retrieve current clock offset.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_GetClockOffset(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to trigger a clock offset estimation procedure.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="inOffset">Allows to specify a custom clock offset to be set.</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_SetClockOffset(std::vector<uint8_t> &outBuffer, uint64_t inOffset = 0, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to enter timesync routine.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_EnterTimeSync(Muse_HW::CommunicationChannel channel, uint8_t* outBuffer);

        /// <summary>Builds command to exit timesync routine.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>      
        static void Cmd_ExitTimeSync(Muse_HW::CommunicationChannel channel, uint8_t* outBuffer);

        /// <summary>Builds command to set a custom sensors full scale configuration.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="gyrFS">Gyroscope full scale code.</param>
        /// <param name="axlFS">Accelerometer full scale code.</param>
        /// <param name="magFS">Magnetometer full scale code.</param>
        /// <param name="hdrFS">High Dynamic Range (HDR) Accelerometer code.</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        static void Cmd_SetSensorsFullScale(Muse_HW::GyroscopeFS gyrFS, Muse_HW::AccelerometerFS axlFS, Muse_HW::MagnetometerFS magFS, Muse_HW::AccelerometerHDRFS hdrFS, std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to retrieve the current sensors full scale configuration.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param>   
        static void Cmd_GetSensorsFullScale(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        // CMD_CALIB_MATRIX = 0x48
        static void Cmd_SetCalibrationMatrix(uint8_t rowId, uint8_t colId, float r1, float r2, float r3, Muse_HW::CommunicationChannel channel, uint8_t* outBuffer);

        static void Cmd_GetCalibrationMatrix(uint8_t rowId, uint8_t colId, Muse_HW::CommunicationChannel channel, uint8_t* outBuffer);

        /// <summary>Builds command to configure a default log mode controlled using the smart button.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="mode">Identifies the set of data to be acquired.</param>
        /// <param name="frequency">Identifies the data acquisition frequency.</param>
        /// <param name="channel">Communication channel over which the command will be sent.</param> 
        static void Cmd_SetButtonLogConfiguration(Muse_HW::DataMode mode, Muse_HW::DataFrequency frequency, std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);


        /// <summary>Builds command to retrieve the current log mode.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param> 
        static void Cmd_GetButtonLogConfiguration(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to retrieve the current user configuration parameters.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="channel">Communication channel over which the command will be sent.</param> 
        static void Cmd_GetUserConfiguration(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Builds command to retrieve the current log mode.</summary>
        /// <returns>A byte array.</returns>
        /// <param name="bitMask">16-bit unsigned integer representing the configuration channels to be set.</param> 
        /// <param name="standby">Boolean flag that allows to enable / disable automatic standby.</param> 
        /// <param name="circular">Boolean flag that allows to enable / disable circular memory management.</param> 
        /// <param name="usbStream">Boolean flag that allows to set default streaming channel to USB.</param> 
        /// <param name="channel">Communication channel over which the command will be sent.</param> 
        static void Cmd_SetUserConfiguration(uint16_t bitMask, bool standby, bool circular, bool usbStream, std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);








        /// <summary>Parse command characteristic to get a command response object.</summary>
        /// <param name="channel">Communication channel over which the command will be sent.</param>
        /// <param name="buffer">Byte array to be parsed.</param>
        /// <param name="response">CommandResponse (output) object reference.</param>
        static void ParseCommandCharacteristic(const uint8_t* inBuffer, CommandResponse* outResponse, Muse_HW::CommunicationChannel channel = Muse_HW::CommunicationChannel::CHANNEL_BLE);

        /// <summary>Decode system state.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="state">Output reference to the SystemState value.</param>
        static Muse_HW::SystemState Dec_SystemState(CommandResponse* response);

        /// <summary>Decode firmware application information.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="crc">Output reference to the Circular Redundancy Check (CRC) as 32-bit unsigned integer.</param>
        /// <param name="length">Output reference to the length of the application firmware (i.e., number of bytes), as a 32-bit unsigned integer.</param>
        // static void Dec_ApplicationInfo(Muse_HW::CommandResponse response, out uint crc, out uint length);

        /// <summary>Decode battery charge level.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="charge">Output reference to battery charge value [%].</param>
        static uint8_t Dec_BatteryCharge(CommandResponse* response);

        /// <summary>Decode battery voltage level.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="voltage">Output reference to battery voltage value [mV].</param>
        // static void Dec_BatteryVoltage(Muse_HW::CommandResponse response, out int voltage);

        /// <summary>Decode check-up register code.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="code">Output reference to check-up register code.</param>
        // static void Dec_CheckUp(Muse_HW::CommandResponse response, out string code);

        /// <summary>Decode firmware version labels.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="boot_rev">Output reference to boot loader firmware version label.</param>
        /// <param name="app_rev">Output reference to application firmware version label.</param>
        // static void Dec_FirmwareVersion(Muse_HW::CommandResponse response, out string boot_rev, out string app_rev);

        /// <summary>Decode date/time.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="datetime">Output reference to date/time object.</param>
        // static void Dec_DateTime(Muse_HW::CommandResponse response, out DateTime datetime);

        /// <summary>Decode device name (i.e., it is the name advertised by Bluetooth module).</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="name">Output reference to device name.</param>
        // static void Dec_DeviceName(Muse_HW::CommandResponse response, out string name);

        /// <summary>Decode device unique identifier.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="id">Output reference to unique identifier.</param>
        // static void Dec_DeviceID(Muse_HW::CommandResponse response, out string id);

        /// <summary>Decode device skills (i.e., hardware features provided by the device).</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="skills">Output reference to hardware skills.</param>
        // static void Dec_DeviceSkills(Muse_HW::CommandResponse response, out List<KeyValuePair<UInt32, string>> skills);

        /// <summary>Decode memory status information.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="available_memory">Output reference to available memory.</param>
        /// <param name="number_of_files">Output reference to number of files currently saved in memory.</param>
        // static void Dec_MemoryStatus(Muse_HW::CommandResponse response, out uint available_memory, out uint number_of_files);

        /// <summary>Decode file information.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="file_info">Output reference to a FileInfo structure.</param>
        // static void Dec_FileInfo(Muse_HW::CommandResponse response, out FileInfo file_info);       

        // static void Dec_ClockOffset(Muse_HW::CommandResponse response, out UInt64 clock_offset);

        /// <summary>Decode sensors full scale / sensitivity configuration.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="sensors_configuration">Output reference to an array of SensorConfig objects (i.e., gyr / axl / mag / hdr).</param>
        static void Dec_SensorFullScales(CommandResponse* response, SensorConfig sensors_configuration[4]);

        // static float[] Dec_CalibrationMatrixValues(byte[] colVal);

        /// <summary>Decode button log configuration.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="mode">Output reference to current acquisition mode configured.</param>
        /// <param name="frequency">Output reference to current acquisition frequency configured.</param>
        // static void Dec_ButtonLogConfiguration(Muse_HW::CommandResponse response, out string mode, out string frequency);

        /// <summary>Decode user configuration packet.</summary>
        /// <param name="response">Input CommandResponse object to be decoded.</param>
        /// <param name="userConfig">Output reference to user configuration object.</param>
        // static void Dec_UserConfiguration(Muse_HW::CommandResponse response, out UserConfig userConfig);

        /// <summary>Returns the data packet dimension corresponding to a given data acquisition mode.</summary>
        /// <returns>A integer value representing the data packet dimension.</returns>
        /// <param name="inMode">Data acquisition mode.</param>
        // static int GetPacketDimension(Muse_HW::DataMode inMode);

        /// <summary>Returns the overall number of data packets contained into a 128-bytes data characteristic given a data acquisition mode.</summary>
        /// <returns>A integer value representing the number of data packets.</returns>
        /// <param name="mode">Data acquisition mode.</param>
        // static int GetNumberOfPackets(Muse_HW::DataMode mode);

        /// <summary>Parse data characteristic to get an observable collection of muse data objects.</summary>
        /// <returns>An observable collection of muse data objects.</returns>
        /// <param name="mode">Data acquisition mode.</param>
        /// <param name="type">Data acquisition type.</param>
        /// <param name="buffer">Byte array to be parsed.</param>
        /// <param name="overall_packet_dimension">Overal packet dimension based on selected acquisition mode.</param>
        /// <param name="num_of_packets">Number of packets into buffer.</param>
        /// <param name="gyr_res">Gyroscope sensitivity coefficient.</param>
        /// <param name="axl_res">Accelerometer sensitivity coefficient.</param>
        /// <param name="mag_res">Magnetometer sensitivity coefficient.</param>
        /// <param name="hdr_res">High Dynamic Range (HDR) Accelerometer sensitivity coefficient.</param>
        // static ObservableCollection<Muse_Data> ParseDataCharacteristic(DataMode mode,
        //                             AcquisitionType type,
        //                             byte[] buffer,
        //                             int overall_packet_dimension,
        //                             int num_of_packets,
        //                             float gyr_res, float axl_res, float mag_res, float hdr_res);    

        /// <summary>Decode data packet.</summary>
        /// <returns>A muse data object.</returns>
        /// <param name="buffer">Byte array to be decoded.</param>
        /// <param name="timestamp">Reference timestamp related to the last notification.</param>
        /// <param name="mode">Data acquisition mode.</param>
        /// <param name="gyr_res">Gyroscope sensitivity coefficient.</param>
        /// <param name="axl_res">Accelerometer sensitivity coefficient.</param>
        /// <param name="mag_res">Magnetometer sensitivity coefficient.</param>
        /// <param name="hdr_res">High Dynamic Range (HDR) Accelerometer sensitivity coefficient.</param>
        // static Muse_Data DecodePacket(byte[] buffer, ulong timestamp, DataMode mode, float gyr_res, float axl_res, float mag_res, float hdr_res);

        /// <summary>Decode Gyroscope reading.</summary>
        /// <returns>An array of float (i.e., x, y, z axes).</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        /// <param name="gyr_res">Gyroscope sensitivity coefficient.</param>
        // static float[] DataTypeGYR(byte[] currentPayload, float gyr_res);

        /// <summary>Decode Accelerometer reading.</summary>
        /// <returns>An array of float (i.e., x, y, z axes).</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        /// <param name="axl_res">Accelerometer sensitivity coefficient.</param>
        // static float[] DataTypeAXL(byte[] currentPayload, float axl_res);

        /// <summary>Decode Magnetometer reading.</summary>
        /// <returns>An array of float (i.e., x, y, z axes).</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        /// <param name="mag_res">Accelerometer sensitivity coefficient.</param>
        // static float[] DataTypeMAGN(byte[] currentPayload, float mag_res);

        /// <summary>Decode High Dynamic Range (HDR) Accelerometer reading.</summary>
        /// <returns>An array of float (i.e., x, y, z axes).</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        /// <param name="hdr_res">Accelerometer sensitivity coefficient.</param>
        // static float[] DataTypeHDR(byte[] currentPayload, float hdr_res);

        /// <summary>Decode orientation (unit) quaternion.</summary>
        /// <returns>An array of float (i.e., qw, qi, qj, qk).</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        // static float[] DataTypeOrientation(byte[] currentPayload);

        /// <summary>Compute Euler Angles given a unit quaternion.</summary>
        /// <returns>An array of float (i.e., roll, pitch and yaw).</returns>
        /// <param name="q">Unit quaternion to be converted.</param>
        // static float[] GetAnglesFromQuaternion(float[] q);

        /// <summary>Decode timestamp.</summary>
        /// <returns>A unsigned long integer value representing the timestamp in epoch format.</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        // static ulong DataTypeTimestamp(byte[] currentPayload);   

        /// <summary>Decode temperature and humidity reading.</summary>
        /// <returns>An array of float (i.e., temperature, humidity).</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        // static float[] DataTypeTempHum(byte[] currentPayload);     

        /// <summary>Decode temperature and barometric pressure reading.</summary>
        /// <returns>An array of float (i.e., temperature, pressure).</returns>
        /// <param name="currentPayload">Bytes array to be decoded.</param>
        // static float[] DataTypeTempPress(byte[] currentPayload);     

        // static Range DataTypeRange(byte[] currentPayload);   

        // static float DataTypeSound(byte[] currentPayload);           

        /// <summary>Decode sensors configuration in terms of full scale and sensitivity.</summary>
        /// <param name="code">24-bit unsigned integer code.</param>
        /// <param name="gyrConfig">Output reference to Gyroscope configuration.</param>
        /// <param name="axlConfig">Output reference to Accelerometer configuration.</param>
        /// <param name="magConfig">Output reference to Magnetometer configuration.</param>
        /// <param name="hdrConfig">Output reference to HDR Accelerometer configuration.</param>
        // static void DecodeMEMSConfiguration(uint code, out SensorConfig gyrConfig, out SensorConfig axlConfig, out SensorConfig magConfig, out SensorConfig hdrConfig);   

        /// <summary>Create a string representation of data acquisition mode.</summary>
        /// <returns>A string.</returns>
        /// <param name="code">32-bit unsigned integer code.</param>
        // static string DataModeToString(UInt32 code); 

        /// <summary>Create a string representation of data acquisition frequency.</summary>
        /// <returns>A string.</returns>
        /// <param name="frequency">8-bit unsigned integer code.</param>
        // static string DataFrequencyToString(byte frequency);                                                                                           
};

#endif