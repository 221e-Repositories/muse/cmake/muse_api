#ifndef RANGE_H
#define RANGE_H

#include <iostream>

using namespace std;

class Range {
    private:
        int lum_vis;
        int lum_ir;
        int lux;

        void setLumVIS(int lum_vis);
        void setLumIR(int lum_ir);
        void setLux(int lux);

    public: 
        Range();
        Range(int lum_vis, int lum_ir, int lux);
        string ToString();

        int getLumVIS();
        int getLumIR();
        int getLux();
};

#endif