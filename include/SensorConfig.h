#ifndef SENSOR_CONFIG_H
#define SENSOR_CONFIG_H

#include <stdint.h>

class SensorConfig {
    private:
        uint32_t full_scale;
        float sensitivity;

    public:
        SensorConfig();

        SensorConfig(uint32_t full_scale, float sensitivity);

        uint32_t getFullScale();
        float getSensitivity();
};

#endif