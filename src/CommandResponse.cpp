#include "CommandResponse.h"

#include <cstring>

CommandResponse::CommandResponse() {
    rx = Muse_HW::Command::CMD_NONE;
    tx = Muse_HW::Command::CMD_NONE;
    len = 0;
    ack = Muse_HW::AcknowledgeType::ACK_NONE;
    // payload
}

CommandResponse::CommandResponse(const uint8_t* inBuffer, size_t bufferSize) {
    if (inBuffer != NULL)
    {
        rx = (Muse_HW::Command)inBuffer[0];
        tx = (Muse_HW::Command)inBuffer[2];

        len = inBuffer[1];
        ack = (Muse_HW::AcknowledgeType)inBuffer[3];

        // payload = new uint8_t[len - 2];
        payload = std::vector<uint8_t>(&inBuffer[4], &inBuffer[bufferSize]);
    }
    else
    {
        rx = Muse_HW::Command::CMD_NONE;
        tx = Muse_HW::Command::CMD_NONE;
        len = 0;
        ack = Muse_HW::AcknowledgeType::ACK_NONE;
        // payload
    }
}

void CommandResponse::setRX(Muse_HW::Command rx) {
    CommandResponse::rx = rx;
}

void CommandResponse::setTX(Muse_HW::Command tx) {
    CommandResponse::tx = tx;
}

void CommandResponse::setLen(int len) {
    CommandResponse::len = len;
}

void CommandResponse::setACK(Muse_HW::AcknowledgeType ack) {
    CommandResponse::ack = ack;
}

void CommandResponse::setPayload(const std::vector<uint8_t> &inBuffer, size_t payload_size) {
    std::copy(inBuffer.begin(), inBuffer.begin()+payload_size, payload.begin()); 
}

Muse_HW::Command CommandResponse::getRX() {
    return rx;
}

Muse_HW::Command CommandResponse::getTX() {
    return tx;
}

int CommandResponse::getLen() {
    return len;
}

Muse_HW::AcknowledgeType CommandResponse::getACK() {
    return ack;
}

std::vector<uint8_t> CommandResponse::getPayload() {
    return payload; 
}