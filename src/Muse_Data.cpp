#include "Muse_Data.h"
#include "Range.h"
#include "Muse_Utils.h"

#include <stdint.h>

Muse_Data::Muse_Data() {

    for (unsigned int i=0; i<Muse_Data::CHANNEL_SIZE_2; i++) {
        th[i] = 0;
        tp[i] = 0;
    }

    for (unsigned int i=0; i<Muse_Data::CHANNEL_SIZE_3; i++) {
        gyr[i] = 0;
        axl[i] = 0;
        mag[i] = 0;
        hdr[i] = 0;

        euler[i] = 0;
    }

    quat[0] = 1;
    for (unsigned int i=1; i<Muse_Data::CHANNEL_SIZE_4; i++) {
        quat[i] = 0;
    }

    range = Range();
    
    sound = 0;
    
    timestamp = 0;
    overall_timestamp = 0;
}

Muse_Data::Muse_Data(float gyr[], float axl[], float mag[], float hdr[], float th[], float tp[], Range range, float sound, float quat[], unsigned long timestamp, unsigned long overall_timestamp) {
    for (unsigned int i=0; i<Muse_Data::CHANNEL_SIZE_2; i++) {
        Muse_Data::th[i] = th[i];
        Muse_Data::tp[i] = tp[i];
    }

    float* euler = NULL;
    Muse_Utils::GetAnglesFromQuaternion(quat, euler);
    for (unsigned int i=0; i<Muse_Data::CHANNEL_SIZE_3; i++) {
        Muse_Data::gyr[i] = gyr[i];
        Muse_Data::axl[i] = axl[i];
        Muse_Data::mag[i] = mag[i];
        Muse_Data::hdr[i] = hdr[i];

        Muse_Data::euler[i] = euler[i];
    }

    Muse_Data::quat[0] = quat[0];
    for (unsigned int i=1; i<Muse_Data::CHANNEL_SIZE_4; i++) {
        Muse_Data::quat[i] = quat[i];
    }

    Muse_Data::range = range;
    
    Muse_Data::sound = sound;
    
    Muse_Data::timestamp = timestamp;
    Muse_Data::overall_timestamp = overall_timestamp;
}

void Muse_Data::setGyroscope(float x, float y, float z) {
    Muse_Data::gyr[0] = x;
    Muse_Data::gyr[1] = y;
    Muse_Data::gyr[2] = z;
}

void Muse_Data::setAccelerometer(float x, float y, float z) {
    Muse_Data::axl[0] = x;
    Muse_Data::axl[1] = y;
    Muse_Data::axl[2] = z;
}

void Muse_Data::setMagnetometer(float x, float y, float z) {
    Muse_Data::mag[0] = x;
    Muse_Data::mag[1] = y;
    Muse_Data::mag[2] = z;
}

void Muse_Data::setHDR(float x, float y, float z) {
    Muse_Data::hdr[0] = x;
    Muse_Data::hdr[1] = y;
    Muse_Data::hdr[2] = z;
}

void Muse_Data::setTH(float temperature, float humidity) {
    Muse_Data::th[0] = temperature;
    Muse_Data::th[1] = humidity;
}

void Muse_Data::setTP(float temperature, float pressure) {
    Muse_Data::tp[0] = temperature;
    Muse_Data::tp[1] = pressure;
}

void Muse_Data::setRange(Range range) {
    Muse_Data::range = range;
}

void Muse_Data::setSound(float sound) {
    Muse_Data::sound = sound;
}

void Muse_Data::setQuaternion(float qw, float qi, float qj, float qk) {
    Muse_Data::quat[0] = qw;
    Muse_Data::quat[1] = qi;
    Muse_Data::quat[2] = qj;
    Muse_Data::quat[3] = qk;

    float* euler = NULL;
    Muse_Utils::GetAnglesFromQuaternion(Muse_Data::quat, euler);
    setEuler(euler[0], euler[1], euler[2]);
}

void Muse_Data::setEuler(float roll, float pitch, float yaw) {
    Muse_Data::euler[0] = roll;
    Muse_Data::euler[1] = pitch;
    Muse_Data::euler[2] = yaw;
}

void Muse_Data::setTimestamp(unsigned long timestamp) {
    Muse_Data::timestamp = timestamp;
}

void Muse_Data::setOverallTimestamp(unsigned long overall_timestamp) {
    Muse_Data::overall_timestamp = overall_timestamp;
}

float* Muse_Data::getGyroscope() {
    return Muse_Data::gyr;
}

float* Muse_Data::getAccelerometer() {
    return Muse_Data::axl;
}

float* Muse_Data::getMagnetometer() {
    return Muse_Data::mag;
}

float* Muse_Data::getHDR() {
    return Muse_Data::hdr;
}

float* Muse_Data::getTH() {
    return Muse_Data::th;
}

float* Muse_Data::getTP() {
    return Muse_Data::tp;
}

Range Muse_Data::getRange() {
    return Muse_Data::range;
}

float Muse_Data::getSound() {
    return Muse_Data::sound;
}

float* Muse_Data::getQuaternion() {
    return Muse_Data::quat;
}

float* Muse_Data::getEuler() {
    return Muse_Data::euler;
}

unsigned long Muse_Data::getTimestamp() {
    return Muse_Data::timestamp;
}

unsigned long Muse_Data::getOverallTimestamp() {
    return Muse_Data::overall_timestamp;
}