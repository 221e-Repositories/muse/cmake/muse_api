#include "Muse_Utils.h"

#define _USE_MATH_DEFINES
#include <math.h>

#include <iostream>
#include <ctime>
#include <cstring>

using namespace std;

void Muse_Utils::GetAnglesFromQuaternion(float q[QUATERNION_DIMENSION], float *outEuler) {
    if (q != NULL) {
        outEuler = new float[EULER_DIMENSION];

        // Compute Euler Angles given a unit quaternion
        outEuler[0] = (float)(atan2(2 * (q[0] * q[1] + q[2] * q[3]), 1 - 2 * (q[1] * q[1] + q[2] * q[2])) * 180 / M_PI);
        outEuler[1] = (float)(asin(2 * q[0] * q[2] - 2 * q[3] * q[1]) * 180 / M_PI);
        outEuler[2] = (float)(atan2(2 * (q[0] * q[3] + q[1] * q[2]), 1 - 2 * (q[2] * q[2] + q[3] * q[3])) * 180 / M_PI);
    }
    else {
        outEuler = NULL;
    }
}

void Muse_Utils::WrapMessage(std::vector<uint8_t> &buffer) {
    size_t n = buffer.size();
    size_t m = n + 4;

    // Resize vector to manage header and trailer
    buffer.resize(m); 

    // Move standard message elements forward of two positions
    std::copy(buffer.begin(), buffer.begin()+n, buffer.begin()+2); 

    // Add header to input buffer
    buffer[0] = '?';
    buffer[1] = '!';
    
    // Add trailer to input buffer
    buffer[m - 2] = '!';
    buffer[m - 1] = '?';
}

void Muse_Utils::ExtractMessage(const uint8_t *inBuffer, size_t bufferSize, std::vector<uint8_t> &outBuffer) {
    // Remove header and trailer from input buffer
    outBuffer = std::vector<uint8_t>(&inBuffer[0], &inBuffer[bufferSize]);
}

void Muse_Utils::Cmd_GetSystemState(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_STATE;
    
    outBuffer.resize(n);

    outBuffer[0] = Muse_HW::Command::CMD_STATE + Muse_HW::READ_BIT_MASK;
    outBuffer[1] = 0;

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_StartStream(Muse_HW::DataMode mode, Muse_HW::DataFrequency frequency, std::vector<uint8_t> &outBuffer, bool enableDirect, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_START_STREAM;
    
    outBuffer.resize(n);

    // Start stream acquisition using set state command
    outBuffer[0] = Muse_HW::Command::CMD_STATE;

    // Set payload length
    outBuffer[1] = Muse_HW::CommandLength::CMD_LENGTH_START_STREAM - 2;

    // Set tx type based on boolean flag value
    if (enableDirect)
        outBuffer[2] = Muse_HW::SystemState::SYS_TX_DIRECT;
    else
        outBuffer[2] = Muse_HW::SystemState::SYS_TX_BUFFERED;

    // Set acquisition mode
    uint8_t *vb = (uint8_t*)&mode;
    std::copy(&vb[0], &vb[sizeof(mode)-1], outBuffer.begin()+3); 

    // Set acquisition frequency
    outBuffer[6] = frequency;

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_StartLog(Muse_HW::DataMode mode, Muse_HW::DataFrequency frequency, std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_START_LOG;
    
    outBuffer.resize(n);

    // Start stream acquisition using set state command
    outBuffer[0] = Muse_HW::Command::CMD_STATE;

    // Set payload length
    outBuffer[1] = Muse_HW::CommandLength::CMD_LENGTH_START_STREAM - 2;

    // Set state - LOG
    outBuffer[2] = Muse_HW::SystemState::SYS_LOG;

    // Set acquisition mode
    uint8_t *vb = (uint8_t*)&mode;
    std::copy(&vb[0], &vb[sizeof(mode)-1], outBuffer.begin()+3); 

    // Set acquisition frequency
    outBuffer[6] = frequency;

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_StopAcquisition(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_STOP_ACQUISITION;
    
    outBuffer.resize(n);

    // Set IDLE state to stop any acquisition procedure
    outBuffer[0] = Muse_HW::Command::CMD_STATE;
    outBuffer[1] = Muse_HW::CommandLength::CMD_LENGTH_STOP_ACQUISITION - 2;
    outBuffer[2] = Muse_HW::SystemState::SYS_IDLE;

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_Restart(std::vector<uint8_t> &outBuffer, Muse_HW::RestartMode restartMode, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_RESTART;
    
    outBuffer.resize(n);

    // Set RESTART command with the specified restart mode (i.e., boot or app)
    outBuffer[0] = Muse_HW::Command::CMD_RESTART;
    outBuffer[1] = Muse_HW::CommandLength::CMD_LENGTH_RESTART - 2;
    outBuffer[2] = restartMode;

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_GetApplicationInfo(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_APP_INFO;
    
    outBuffer.resize(n);

    // Get firmware application info
    outBuffer[0] = Muse_HW::Command::CMD_APP_INFO + Muse_HW::READ_BIT_MASK;
    
    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_GetBatteryCharge(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_BATTERY_CHARGE;
    
    outBuffer.resize(n);
    
    // Get battery charge
    outBuffer[0] =  Muse_HW::Command::CMD_BATTERY_CHARGE +  Muse_HW::READ_BIT_MASK;
    
    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_GetBatteryVoltage(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_BATTERY_VOLTAGE;
    
    outBuffer.resize(n);

    // Get battery voltage
    outBuffer[0] =  Muse_HW::Command::CMD_BATTERY_VOLTAGE +  Muse_HW::READ_BIT_MASK;

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_GetDeviceCheckup(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_CHECK_UP;
    
    outBuffer.resize(n);

    // Get check up register value
    outBuffer[0] =  Muse_HW::Command::CMD_CHECK_UP +  Muse_HW::READ_BIT_MASK;
    
    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_GetFirmwareVersion(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_FW_VERSION;
    
    outBuffer.resize(n);

    // Get firmware version labels (i.e., bootloader and application firmware)
    outBuffer[0] = Muse_HW::Command::CMD_FW_VERSION + Muse_HW::READ_BIT_MASK;

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_SetTime(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_SET_TIME;
    
    outBuffer.resize(n);

    // Set time using current timespan since 1970
    outBuffer[0] = Muse_HW::Command::CMD_TIME;
    outBuffer[1] = Muse_HW::CommandLength::CMD_LENGTH_SET_TIME - 2;

    // Get current timespan since 1/1/1970
    time_t timeSpan = time(0); // DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
    uint32_t secondsSinceEpoch = (uint32_t)timeSpan;

    // Set payload - seconds since epoch (4 bytes)
    uint8_t *vb = (uint8_t*)&secondsSinceEpoch;
    std::copy(&vb[0], &vb[sizeof(secondsSinceEpoch)], outBuffer.begin()+2); 

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_GetTime(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_TIME;
    
    outBuffer.resize(n);

    // Get current datetime
    outBuffer[0] = Muse_HW::Command::CMD_TIME + Muse_HW::READ_BIT_MASK;

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_GetDeviceName(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_BLE_NAME;
    
    outBuffer.resize(n);
            
    // Get device name (i.e., ble name)
    outBuffer[0] = Muse_HW::Command::CMD_BLE_NAME + Muse_HW::READ_BIT_MASK;

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

// // void Muse_Utils::Cmd_SetDeviceName(const string &bleName, Muse_HW::CommunicationChannel channel, uint8_t* outBuffer) {
// // }

void Muse_Utils::Cmd_GetDeviceID(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_DEVICE_ID;
    
    outBuffer.resize(n);
    
    // Get device unique identifier
    outBuffer[0] = Muse_HW::Command::CMD_DEVICE_ID + Muse_HW::READ_BIT_MASK;

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_GetDeviceSkills(std::vector<uint8_t> &outBuffer, bool softwareSkills, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_DEVICE_SKILLS;
    
    outBuffer.resize(n);

    // Get device skills (i.e., hardware or software based on specified flag)
    outBuffer[0] = Muse_HW::Command::CMD_DEVICE_SKILLS + Muse_HW::READ_BIT_MASK;
    outBuffer[1] = Muse_HW::CommandLength::CMD_LENGTH_GET_DEVICE_SKILLS - 2;

    if (softwareSkills)
        outBuffer[2] = 0x01;

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_GetMemoryStatus(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_MEM_CONTROL;
    
    outBuffer.resize(n);

    // Get available memory
    outBuffer[0] = Muse_HW::Command::CMD_MEM_CONTROL + Muse_HW::READ_BIT_MASK;
    
    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_EraseMemory(std::vector<uint8_t> &outBuffer, uint8_t bulk_erase, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_SET_MEM_CONTROL;
    
    outBuffer.resize(n);

    // Erase memory 
    outBuffer[0] = Muse_HW::Command::CMD_MEM_CONTROL;
    outBuffer[1] = Muse_HW::CommandLength::CMD_LENGTH_SET_MEM_CONTROL - 2;
    outBuffer[2] = bulk_erase;

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_MemoryFileInfo(std::vector<uint8_t> &outBuffer, unsigned short fileId, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_MEM_FILE_INFO;
    
    outBuffer.resize(n);
    
    // Retrieve file information given a specific file identifier
    outBuffer[0] = Muse_HW::Command::CMD_MEM_FILE_INFO + Muse_HW::READ_BIT_MASK;
    outBuffer[1] = Muse_HW::CommandLength::CMD_LENGTH_GET_MEM_FILE_INFO - 2;

    // Set file id as a 2-bytes unsigned integer value
    uint8_t *vb = (uint8_t*)&fileId;
    std::copy(&vb[0], &vb[sizeof(fileId)], outBuffer.begin()+2); 
    // outBuffer[2] = vb[0];
    // outBuffer[3] = vb[1];

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_MemoryFileDownload(std::vector<uint8_t> &outBuffer, unsigned short fileId, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_MEM_FILE_DOWNLOAD;
    
    outBuffer.resize(n);

    // Start file offload procedure
    outBuffer[0] = Muse_HW::Command::CMD_MEM_FILE_DOWNLOAD;
    outBuffer[1] = Muse_HW::CommandLength::CMD_LENGTH_GET_MEM_FILE_DOWNLOAD - 2;

    // Set file identifier
    uint8_t *vb = (uint8_t*)&fileId;
    std::copy(&vb[0], &vb[sizeof(fileId)], outBuffer.begin()+2); 
    // outBuffer[2] = vb[0];
    // outBuffer[3] = vb[1];

    // Set file offload channel (USB vs BLE)
    outBuffer[4] = channel;
    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB)
        outBuffer[4] = 0x00; // set by default to USB channel (if 0x01 it manages the BLE transfer)

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_GetClockOffset(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_CLK_OFFSET;
    
    outBuffer.resize(n);

    // Get clock offset
    outBuffer[0] = Muse_HW::Command::CMD_CLK_OFFSET + Muse_HW::READ_BIT_MASK;
    
    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_SetClockOffset(std::vector<uint8_t> &outBuffer, uint64_t inOffset, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_SET_CLK_OFFSET;
    
    outBuffer.resize(n);

    // Set clock offset
    outBuffer[0] = Muse_HW::Command::CMD_CLK_OFFSET;
    outBuffer[1] = Muse_HW::CommandLength::CMD_LENGTH_SET_CLK_OFFSET - 2;

    uint8_t *vb = (uint8_t*)&inOffset;
    std::copy(&vb[0], &vb[sizeof(inOffset)], outBuffer.begin()+2); 
   
    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

// void Muse_Utils::Cmd_EnterTimeSync(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
//     size_t n = Muse_HW::CommandLength::CMD_LENGTH_ENTER_TIME_SYNC;
    
//     outBuffer.resize(n);

//     if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
//         Muse_Utils::WrapMessage(outBuffer);
// }
 
// void Muse_Utils::Cmd_ExitTimeSync(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
//     size_t n = Muse_HW::CommandLength::CMD_LENGTH_EXIT_TIME_SYNC;
    
//     outBuffer.resize(n);

//     if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
//         Muse_Utils::WrapMessage(outBuffer);
// }

void Muse_Utils::Cmd_SetSensorsFullScale(Muse_HW::GyroscopeFS gyrFS, Muse_HW::AccelerometerFS axlFS, Muse_HW::MagnetometerFS magFS, Muse_HW::AccelerometerHDRFS hdrFS, std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_SET_SENSORS_FS;
    
    outBuffer.resize(n);

    // Set sensors full scale configuration
    outBuffer[0] = Muse_HW::Command::CMD_SENSORS_FS;
    outBuffer[1] = Muse_HW::CommandLength::CMD_LENGTH_SET_SENSORS_FS - 2;

    // Build integer configuration code to be sent
    outBuffer[2] = (axlFS | gyrFS | hdrFS | magFS);
    
    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_GetSensorsFullScale(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_SENSORS_FS;
    
    outBuffer.resize(n);

    // Get current sensors full scale configuration
    outBuffer[0] = Muse_HW::Command::CMD_SENSORS_FS + Muse_HW::READ_BIT_MASK;

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

// void Muse_Utils::Cmd_SetCalibrationMatrix(uint8_t rowId, uint8_t colId, float r1, float r2, float r3, std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
//     size_t n = Muse_HW::CommandLength::CMD_LENGTH_SET_CALIB_MATRIX;
    
//     outBuffer.resize(n);

//     if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
//         Muse_Utils::WrapMessage(outBuffer);
// }

// void Muse_Utils::Cmd_GetCalibrationMatrix(uint8_t rowId, uint8_t colId, std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
//     size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_CALIB_MATRIX;
    
//     outBuffer.resize(n);

//     if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
//         Muse_Utils::WrapMessage(outBuffer);
// }

void Muse_Utils::Cmd_SetButtonLogConfiguration(Muse_HW::DataMode mode, Muse_HW::DataFrequency frequency, std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_SET_BUTTON_LOG;
    
    outBuffer.resize(n);

    // Set log mode command
    outBuffer[0] = Muse_HW::Command::CMD_BUTTON_LOG;
    outBuffer[1] = Muse_HW::CommandLength::CMD_LENGTH_SET_BUTTON_LOG - 2;

    // Set log mode code
    uint8_t *vb = (uint8_t*)&mode;
    std::copy(&vb[0], &vb[sizeof(mode)-1], outBuffer.begin()+2); 

    // Set log frequency
    outBuffer[5] = frequency;
    
    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_GetButtonLogConfiguration(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_BUTTON_LOG;
    
    outBuffer.resize(n);

    // Get current log mode
    outBuffer[0] = Muse_HW::Command::CMD_BUTTON_LOG + Muse_HW::READ_BIT_MASK;
    
    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_GetUserConfiguration(std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_GET_USER_CONFIG;
    
    outBuffer.resize(n);

    // Get current user configuration
    outBuffer[0] = Muse_HW::Command::CMD_USER_CONFIG + Muse_HW::READ_BIT_MASK;
    
    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}

void Muse_Utils::Cmd_SetUserConfiguration(uint16_t bitMask, bool standby, bool circular, bool usbStream, std::vector<uint8_t> &outBuffer, Muse_HW::CommunicationChannel channel) {
    size_t n = Muse_HW::CommandLength::CMD_LENGTH_SET_USER_CONFIG;
    
    outBuffer.resize(n);

    // Set a custom user configuration
    outBuffer[0] = Muse_HW::Command::CMD_USER_CONFIG;
    outBuffer[1] = Muse_HW::CommandLength::CMD_LENGTH_SET_USER_CONFIG - 2;

    // Set bit-mask from UserConfigMask enum
    uint8_t *vb = (uint8_t*)&bitMask;
    std::copy(&vb[0], &vb[sizeof(bitMask)], outBuffer.begin()+2); 

    // Build configuration code to be sent using user input
    uint16_t config_code = 0;
    if (standby)
        config_code |= 0x0001;

    if (circular)
        config_code |= 0x0002;

    if (usbStream)
        config_code |= 0x0004;

    // Set configuration code
    vb = (uint8_t*)&config_code;
    std::copy(&vb[0], &vb[sizeof(config_code)], outBuffer.begin()+4); 

    if (channel == Muse_HW::CommunicationChannel::CHANNEL_USB) 
        Muse_Utils::WrapMessage(outBuffer);
}


void Muse_Utils::ParseCommandCharacteristic(const uint8_t* inBuffer, CommandResponse* outResponse, Muse_HW::CommunicationChannel channel) {
    // Build a command response container given the command characteristic content as a byte array
    // Manage also header and trailer removal in case of BLE or USB channel
    if (channel == Muse_HW::CommunicationChannel::CHANNEL_BLE)
        outResponse = new CommandResponse(inBuffer, 20);
    else {
        std::vector<uint8_t> buffer;
        ExtractMessage(inBuffer, 20, buffer);
        outResponse = new CommandResponse(buffer.data(), 20);
    }
}

Muse_HW::SystemState Muse_Utils::Dec_SystemState(CommandResponse* response) {
    Muse_HW::SystemState state = Muse_HW::SystemState::SYS_NONE;

    // Decode battery charge percentage value given command response payload
    if ((response->getTX() & 0x7F) == Muse_HW::Command::CMD_STATE &&
        response->getACK() == Muse_HW::AcknowledgeType::ACK_SUCCESS)
    {
        if ((response->getTX() & Muse_HW::READ_BIT_MASK) != 0)
        {
            std::vector<uint8_t> payload = response->getPayload();
            
            state = (Muse_HW::SystemState)payload[0];
        }
    }

    return state;
}

uint8_t Muse_Utils::Dec_BatteryCharge(CommandResponse* response) {
    uint8_t charge = -1;

    // Decode battery charge percentage value given command response payload
    if ((response->getTX() & 0x7F) == Muse_HW::Command::CMD_BATTERY_CHARGE &&
        response->getACK() == Muse_HW::AcknowledgeType::ACK_SUCCESS)
    {
        if ((response->getTX() & Muse_HW::READ_BIT_MASK) != 0)
        {
            std::vector<uint8_t> payload = response->getPayload();
            
            charge = payload[0];
        }
    }

    return charge;
}

void Muse_Utils::Dec_SensorFullScales(CommandResponse* response, SensorConfig sensors_configuration[4]) {

    // Decode sensors configuration
    if (((response->getTX() & 0x7F) == Muse_HW::Command::CMD_SENSORS_FS || 
         ((response->getTX() & 0x7F) == Muse_HW::Command::CMD_STATE && response->getLen() == 0x09)) &&
        response->getACK() == Muse_HW::AcknowledgeType::ACK_SUCCESS)
    {
        // Get payload buffer as vector<uint8_t>
        std::vector<uint8_t> payload = response->getPayload();

        uint32_t value = 0;

        value |= 0; // payload[0] << 24;
        value |= payload[2] << 16;
        value |= payload[1] << 8;
        value |= payload[0];
            
        // Decode full scales and corresponding sensitivity coefficients
        uint8_t gyrCode = value & 0x03;
        uint8_t axlCode = value & 0x0c;
        uint8_t magCode = value & 0xc0;
        uint8_t hdrCode = value & 0x30;

        switch(gyrCode) {
            case Muse_HW::GyroscopeFS::GYR_FS_245dps: // 0x00
            sensors_configuration[0] = SensorConfig(245,0.00875);
            break;
            case Muse_HW::GyroscopeFS::GYR_FS_500dps: // 0x01
            sensors_configuration[0] = SensorConfig(500, 0.0175);
            break;
            case Muse_HW::GyroscopeFS::GYR_FS_1000dps: // 0x02
            sensors_configuration[0] = SensorConfig(1000, 0.035);
            break;
            case Muse_HW::GyroscopeFS::GYR_FS_2000dps: // 0x03
            sensors_configuration[0] = SensorConfig(2000, 0.070);
            break;
            default:
            break;
        }

        switch(axlCode) {
            case Muse_HW::AccelerometerFS::AXL_FS_4g: // 0x00
            sensors_configuration[1] = SensorConfig(4, 0.122f);
            break;
            case Muse_HW::AccelerometerFS::AXL_FS_08g: // 0x08
            sensors_configuration[1] = SensorConfig(8, 0.244f);
            break;
            case Muse_HW::AccelerometerFS::AXL_FS_16g: // 0x0c
            sensors_configuration[1] = SensorConfig(16, 0.488f);
            break;
            case Muse_HW::AccelerometerFS::AXL_FS_32g: // 0x04
            sensors_configuration[1] = SensorConfig(32, 0.976f);
            break;
            default:
            break;
        }

        switch(magCode) {
            case Muse_HW::MagnetometerFS::MAG_FS_04G: // 0x00
            sensors_configuration[2] = SensorConfig(4, 1000.0f/6842.0f);
            break;
            case Muse_HW::MagnetometerFS::MAG_FS_08G: // 0x40
            sensors_configuration[2] = SensorConfig(8, 1000.0f/3421.0f);
            break;
            case Muse_HW::MagnetometerFS::MAG_FS_12G: // 0x80
            sensors_configuration[2] = SensorConfig(12, 1000.0f/2281.0f);
            break;
            case Muse_HW::MagnetometerFS::MAG_FS_16G: // 0xc0
            sensors_configuration[2] = SensorConfig(16, 1000.0f/1711.0f);
            break;
            default:
            break;
        }

        switch(hdrCode) {
            case Muse_HW::AccelerometerHDRFS::HDR_FS_100g: // 0x00
            sensors_configuration[3] = SensorConfig(100, 49.0f);
            break;
            case Muse_HW::AccelerometerHDRFS::HDR_FS_200g: // 0x10
            sensors_configuration[3] = SensorConfig(200, 98.0f);
            break;
            case Muse_HW::AccelerometerHDRFS::HDR_FS_400g: // 0x30
            sensors_configuration[3] = SensorConfig(400, 195.0f);
            break;
            default:
            break;
        }
     
    }
}