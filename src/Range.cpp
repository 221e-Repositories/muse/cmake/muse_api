#include "Range.h"

Range::Range() {
    lum_vis = 0;
    lum_ir = 0;
    lux = 0;
}

Range::Range(int arg1, int arg2, int arg3) {
    lum_vis = arg1;
    lum_ir = arg2;
    lux = arg3;
}

void Range::setLumVIS(int lum_vis) {
    Range::lum_vis = lum_vis;
}

void Range::setLumIR(int lum_ir) {
    Range::lum_ir = lum_ir;
}

void Range::setLux(int lux) {
    Range::lux = lux;
}

int Range::getLumVIS() {
    return lum_vis;
}

int Range::getLumIR() {
    return lum_ir;
}

int Range::getLux() {
    return lux;
}

string Range::ToString() {
    string str = lux + "\t" + lum_vis;
    str += "\t" + lum_ir;

    return str;
}