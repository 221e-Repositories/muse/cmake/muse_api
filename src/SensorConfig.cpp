#include "SensorConfig.h"

SensorConfig::SensorConfig() {
    full_scale = 0;
    sensitivity = 1;
}

SensorConfig::SensorConfig(uint32_t full_scale, float sensitivity) {
    SensorConfig::full_scale = full_scale;
    SensorConfig::sensitivity = sensitivity;
}

uint32_t SensorConfig::getFullScale() {
    return full_scale;
}

float SensorConfig::getSensitivity() {
    return sensitivity;
}