#include <iostream>
#include <cstring>
#include <vector>
#include <thread>
#include <gattlib.h>

#include <assert.h>
#include <glib.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

#include "Muse_HW.h"
#include "Muse_Utils.h"
#include "CommandResponse.h"

#ifdef GATTLIB_LOG_BACKEND_SYSLOG
#include <syslog.h>
#endif

#define BLE_SCAN_TIMEOUT 4

static uuid_t g_notify_cmd_uuid;
static uuid_t g_notify_dat_uuid;

static uuid_t g_write_uuid;

static GMainLoop *m_main_loop;

static SensorConfig sensors_configuration[4];
static bool enable_data_visualization;

int ret;
int argid;
gatt_connection_t* connection;
std::vector<uint8_t> buffer;
uint8_t* msg = NULL;
uint8_t msg_size = 0;
Muse_HW::DataMode mode;

void discoveredDevice(void *adapter, const char *addr, const char *name, void *user_data)
{
    if (name)
        printf("Discovered %s - '%s'\n", addr, name);
    else
        printf("Discovered %s\n", addr);
}

bool scan()
{
    void *adapter;
    bool scanned = true;

    int res = gattlib_adapter_open(NULL, &adapter);

    if (res) {
        puts("Failed to open adapter.");
        scanned = false;
    }

    res = gattlib_adapter_scan_enable(adapter, discoveredDevice, BLE_SCAN_TIMEOUT, NULL /* user_data */);
    if (res) {
        puts("Failed to scan.");
        scanned = false;
    }

    gattlib_adapter_scan_disable(adapter);

    puts("Scan completed");

    return scanned;
}

void notification_data_handler(const uuid_t* uuid, const uint8_t* data, size_t data_length, void* user_data) {
	// printf("Data Handler: ");

	// Print out raw data buffer
	// for (int i = 0; i < data_length; i++) {
	// 	printf("%02x ", data[i]);
	// }
	// printf("\n");

	if (enable_data_visualization) {
		// Decode IMU packet
		uint8_t offset = 0;

		uint64_t timestamp;
		std::memcpy(&timestamp, data, 8);

		offset += 8;

		// Decode gyr
		float gyr[] = {0,0,0};
		// float gyr_res = 0.00875;
		for (uint8_t i=0; i<3; i++) {
			int16_t val;
			std::memcpy(&val, data+offset+2*i, 2);
			gyr[i] = val * sensors_configuration[0].getSensitivity();
		}

		offset += 6;

		// Decode axl
		float axl[] = {0,0,0};
		// float axl_res = 0.122;
		for (uint8_t i=0; i<3; i++) {
			int16_t val;
			std::memcpy(&val, data+offset+2*i, 2);
			axl[i] = val * sensors_configuration[1].getSensitivity();
		}

		printf("%f,%f,%f\t%f,%f,%f\n ", gyr[0], gyr[1], gyr[2], axl[0], axl[1], axl[2]);
	}
}

void notification_handler(const uuid_t* uuid, const uint8_t* data, size_t data_length, void* user_data) {
	// printf("Command Handler: ");

	// for (int i = 0; i < data_length; i++) {
	// 	printf("%02x ", data[i]);
	// }
	// printf("\n");

	CommandResponse* response = new CommandResponse(data, data_length);

	if (response->getACK() == 0) {

		bool read = response->getTX() & 0x80 != 0;

		// Manage response decoding in the case of positive ack
		switch(response->getTX() & 0x7F){
			case Muse_HW::Command::CMD_NONE: // 0xff
			break;
			case Muse_HW::Command::CMD_ACK: // 0x00
			break;
			case Muse_HW::Command::CMD_STATE: // 0x02
			if (read) {
				// Decode current system state as provided by the device
				Muse_HW::SystemState state = Muse_Utils::Dec_SystemState(response);
				printf("%d\n", state);
			}
			else {
				// Manage support variables on set system state response (e.g., full scale / sensitivity on start streaming)
				if (response->getTX() == Muse_HW::Command::CMD_STATE &&
					response->getLen() == 0x09) {
					// Acknowledge on start streaming command. Example: 
					// ACK  LEN  CMD  ERR  FULL SCALE CODE  ACQUISITION MODE  ACQUISITION FREQUENCY
					// 00   09   02   00   b7 00 00         03 00 00          01                     00 00 00 00 00 00 00 00 00

					// Decode current sensors configuration (full scale
					Muse_Utils::Dec_SensorFullScales(response, sensors_configuration);

					// Decode selected acquisition mode, for confirmation only
					// ... TO BE IMPLEMENTED ... 

					// Decode selected acquisition frequency, for confirmation only
					// ... TO BE IMPLEMENTED ...
					
					enable_data_visualization = true;

					gattlib_register_notification(connection, notification_data_handler, NULL);
					ret = gattlib_notification_start(connection, &g_notify_dat_uuid);
					if (ret) {
						printf("Fail to start notification on data.");
						// goto DISCONNECT;
					}
				}
			}
			break;
			case Muse_HW::Command::CMD_RESTART: // 0x03
			break;
			case Muse_HW::Command::CMD_APP_INFO: // 0x04
			break;
			case Muse_HW::Command::CMD_BATTERY_CHARGE: // 0x07
			if (read) {
				uint8_t charge_level = Muse_Utils::Dec_BatteryCharge(response);
				printf("%d\n", charge_level);
			}
			break;
			case Muse_HW::Command::CMD_BATTERY_VOLTAGE: // 0x08
			break;
			case Muse_HW::Command::CMD_CHECK_UP: // 0x09
			
			break;
			case Muse_HW::Command::CMD_FW_VERSION: // 0x0a
			break;
			case Muse_HW::Command::CMD_TIME: // 0x0b
			break;
			case Muse_HW::Command::CMD_BLE_NAME: // 0x0c
			break;
			case Muse_HW::Command::CMD_DEVICE_ID: // 0x0e
			break;
			case Muse_HW::Command::CMD_DEVICE_SKILLS: // 0x0f
			break;
			case Muse_HW::Command::CMD_MEM_CONTROL: // 0x20         
			break;
			case Muse_HW::Command::CMD_MEM_FILE_INFO: // 0x21       
			break;
			case Muse_HW::Command::CMD_MEM_FILE_DOWNLOAD: // 0x22
			break;
			case Muse_HW::Command::CMD_CLK_OFFSET: // 0x31          
			break;
			case Muse_HW::Command::CMD_TIME_SYNC: // 0x32         
			break;
			case Muse_HW::Command::CMD_EXIT_TIME_SYNC: // 0x33
			break;
			case Muse_HW::Command::CMD_SENSORS_FS: // 0x40
			if (read) {

			}
			else {

			}
			break;
			case Muse_HW::Command::CMD_CALIB_MATRIX: // 0x48
			break;
			case Muse_HW::Command::CMD_BUTTON_LOG: // 0x50
			break;
			case Muse_HW::Command::CMD_USER_CONFIG: // 0x51
			break;
			default:
			break;
		}
	}
	else {
		// Manage error notification in the case of negative ack (i.e., ack = 0x01)
		printf("ERROR: negative acknowledge on response!");
	}
}

static void on_user_abort(int arg) {
	g_main_loop_quit(m_main_loop);
}

static void usage(char *argv[]) {
	printf("%s <device_address> <notification_characteristic_uuid> [<write_characteristic_uuid> <write_characteristic_hex_data> ...]\n", argv[0]);
}


int main(int argc, char *argv[]) {

	enable_data_visualization = false;

	///////////////////////////////////////////////////////////////////////////
	// Required only the first time to retrieve MAC address of interest
	scan();

	const char *mac_address = "00:80:E1:26:D5:46";
	const char *cmd_uuid = "d5913036-2d8a-41ee-85b9-4e361aa5c8a7";	// Command characteristic UUID
	const char *dat_uuid = "09bf2c52-d1d9-c0b7-4145-475964544307";	// Data characteristic UUID

	// Check UUID consistency before enabling notification
	gattlib_string_to_uuid(cmd_uuid, strlen(cmd_uuid) + 1, &g_notify_cmd_uuid);
	gattlib_string_to_uuid(dat_uuid, strlen(dat_uuid) + 1, &g_notify_dat_uuid);

	///////////////////////////////////////////////////////////////////////////


#ifdef GATTLIB_LOG_BACKEND_SYSLOG
	openlog("gattlib_notification", LOG_CONS | LOG_NDELAY | LOG_PERROR | LOG_PID, LOG_USER);
	setlogmask(LOG_UPTO(LOG_DEBUG));
#endif

	connection = gattlib_connect(NULL, mac_address, GATTLIB_CONNECTION_OPTIONS_LEGACY_DEFAULT);
	if (connection == NULL) {
		// GATTLIB_LOG(GATTLIB_ERROR, "Fail to connect to the bluetooth device.");
        printf("Fail to connect to the bluetooth device.");
		return 1;
	}

	gattlib_register_notification(connection, notification_handler, NULL);
	

#ifdef GATTLIB_LOG_BACKEND_SYSLOG
	openlog("gattlib_notification", LOG_CONS | LOG_NDELAY | LOG_PERROR, LOG_USER);
	setlogmask(LOG_UPTO(LOG_DEBUG));
#endif

	ret = gattlib_notification_start(connection, &g_notify_cmd_uuid);
	if (ret) {
        printf("Fail to start notification on command.");
		goto DISCONNECT;
	}

	// Get system state 
	// Muse_Utils::Cmd_GetSystemState(buffer);

	// msg = buffer.data();
    // msg_size = buffer.size();

	// ret = gattlib_write_char_by_uuid(connection, &g_notify_cmd_uuid, msg, msg_size);
	// if (ret != GATTLIB_SUCCESS) {
	// 	if (ret == GATTLIB_NOT_FOUND) {
	// 		printf("Could not find GATT Characteristic with UUID %s.", cmd_uuid);
	// 	} else {
	// 		printf("Error while writing GATT Characteristic with UUID %s (ret:%d)",
	// 			cmd_uuid, ret);
	// 	}
	// 	goto DISCONNECT;
	// }

	// // Get sensors configuration (full scales) 
	// Muse_Utils::Cmd_GetSensorsFullScale(buffer);

	// msg = buffer.data();
    // msg_size = buffer.size();

	// ret = gattlib_write_char_by_uuid(connection, &g_notify_cmd_uuid, msg, msg_size);
	// if (ret != GATTLIB_SUCCESS) {
	// 	if (ret == GATTLIB_NOT_FOUND) {
	// 		printf("Could not find GATT Characteristic with UUID %s.", cmd_uuid);
	// 	} else {
	// 		printf("Error while writing GATT Characteristic with UUID %s (ret:%d)",
	// 			cmd_uuid, ret);
	// 	}
	// 	goto DISCONNECT;
	// }

	// Start IMU streaming 
	mode = Muse_HW::DataMode::DATA_MODE_IMU;
	// Muse_Utils::Cmd_StartStream(mode, Muse_HW::DataFrequency::DATA_FREQ_25Hz, buffer);	// Buffered
	Muse_Utils::Cmd_StartStream(mode, Muse_HW::DataFrequency::DATA_FREQ_25Hz, buffer, true); 	// Direct

	msg = buffer.data();
    msg_size = buffer.size();

	ret = gattlib_write_char_by_uuid(connection, &g_notify_cmd_uuid, msg, msg_size);
	if (ret != GATTLIB_SUCCESS) {
		if (ret == GATTLIB_NOT_FOUND) {
			printf("Could not find GATT Characteristic with UUID %s.", cmd_uuid);
		} else {
			printf("Error while writing GATT Characteristic with UUID %s (ret:%d)",
				cmd_uuid, ret);
		}
		goto DISCONNECT;
	}

	// Catch CTRL-C
	signal(SIGINT, on_user_abort);

	m_main_loop = g_main_loop_new(NULL, 0);
	g_main_loop_run(m_main_loop);

	// In case we quit the main loop, clean the connection
	gattlib_notification_stop(connection, &g_notify_cmd_uuid);
	g_main_loop_unref(m_main_loop);

DISCONNECT:
	// Stop acquisition before disconnect device 
	Muse_Utils::Cmd_StopAcquisition(buffer);

	msg = buffer.data();
    msg_size = buffer.size();
	
	ret = gattlib_write_char_by_uuid(connection, &g_notify_cmd_uuid, msg, msg_size);

	// Disconnect device
	gattlib_disconnect(connection);
	puts("Done");
	return ret;
}

///////////////////////////////////////////////////////////////////////////////
//     // TEST COMMAND STRUCTURE EXAMPLES

//     // std::vector<uint8_t> outBuffer;
    
//     // Muse_Utils::Cmd_GetSystemState(outBuffer, Muse_HW::CommunicationChannel::CHANNEL_USB);
    
//     // Muse_HW::DataMode mode = (Muse_HW::DataMode)(Muse_HW::DataMode::DATA_MODE_IMU | Muse_HW::DataMode::DATA_MODE_HDR);
//     // Muse_Utils::Cmd_StartStream(mode, Muse_HW::DataFrequency::DATA_FREQ_100Hz, outBuffer, false, Muse_HW::CommunicationChannel::CHANNEL_USB);

//     // Muse_HW::DataMode mode = (Muse_HW::DataMode)(Muse_HW::DataMode::DATA_MODE_IMU | Muse_HW::DataMode::DATA_MODE_HDR | Muse_HW::DataMode::DATA_MODE_TIMESTAMP);
//     // Muse_Utils::Cmd_StartLog(mode, Muse_HW::DataFrequency::DATA_FREQ_100Hz, outBuffer, Muse_HW::CommunicationChannel::CHANNEL_USB);

//     // Muse_Utils::Cmd_StopAcquisition(outBuffer, Muse_HW::CommunicationChannel::CHANNEL_USB);

//     // Muse_Utils::Cmd_Restart(outBuffer, Muse_HW::RestartMode::BOOT, Muse_HW::CommunicationChannel::CHANNEL_BLE);

//     // Muse_Utils::Cmd_GetApplicationInfo(outBuffer, Muse_HW::CommunicationChannel::CHANNEL_USB);

//     // Muse_Utils::Cmd_GetBatteryCharge(outBuffer, Muse_HW::CommunicationChannel::CHANNEL_USB);
//     // Muse_Utils::Cmd_GetBatteryVoltage(outBuffer);
//     // Muse_Utils::Cmd_GetDeviceCheckup(outBuffer, Muse_HW::CommunicationChannel::CHANNEL_BLE);

//     // Muse_Utils::Cmd_GetFirmwareVersion(outBuffer);
//     // Muse_Utils::Cmd_GetTime(outBuffer, Muse_HW::CommunicationChannel::CHANNEL_USB);

//     // Muse_Utils::Cmd_SetTime(outBuffer, Muse_HW::CommunicationChannel::CHANNEL_USB);

//     // Muse_Utils::Cmd_GetDeviceName(outBuffer, Muse_HW::CommunicationChannel::CHANNEL_USB);
//     // Muse_Utils::Cmd_GetDeviceID(outBuffer, Muse_HW::CommunicationChannel::CHANNEL_USB);
//     // Muse_Utils::Cmd_GetDeviceSkills(outBuffer, true);

//     // Muse_Utils::Cmd_GetMemoryStatus(outBuffer);
//     // Muse_Utils::Cmd_EraseMemory(outBuffer);

//     // Muse_Utils::Cmd_MemoryFileInfo(outBuffer, 40);
//     // Muse_Utils::Cmd_MemoryFileDownload(outBuffer, 367, Muse_HW::CommunicationChannel::CHANNEL_USB);

//     // Muse_Utils::Cmd_GetClockOffset(outBuffer);
//     // Muse_Utils::Cmd_SetClockOffset(outBuffer, 1675785414999, Muse_HW::CommunicationChannel::CHANNEL_USB);

//     // Muse_Utils::Cmd_SetSensorsFullScale(Muse_HW::GyroscopeFS::GYR_FS_1000dps, Muse_HW::AccelerometerFS::AXL_FS_16g, Muse_HW::MagnetometerFS::MAG_FS_12G, Muse_HW::AccelerometerHDRFS::HDR_FS_200g, outBuffer);
//     // Muse_Utils::Cmd_GetSensorsFullScale(outBuffer);

//     // Muse_Utils::Cmd_SetButtonLogConfiguration(Muse_HW::DataMode::DATA_MODE_9DOF, Muse_HW::DataFrequency::DATA_FREQ_1600Hz, outBuffer, Muse_HW::CommunicationChannel::CHANNEL_USB);
//     // Muse_Utils::Cmd_GetButtonLogConfiguration(outBuffer);

//     // Muse_Utils::Cmd_GetUserConfiguration(outBuffer);
    
//     /*
//     uint16_t bitMask = Muse_HW::UserConfigMask::USER_CFG_MASK_USB_STREAM | Muse_HW::UserConfigMask::USER_CFG_MASK_STANDBY;
//     bool standby = false;
//     bool circular = false; 
//     bool usbStream = true;
//     Muse_Utils::Cmd_SetUserConfiguration(bitMask, standby, circular, usbStream, outBuffer, Muse_HW::CommunicationChannel::CHANNEL_USB);
//     */
   
//     // Print out the built command
//     // for (size_t i = 0; i < outBuffer.size(); i++) 
//     //     std::cout << std::hex << static_cast<int>(outBuffer[i]) << ' ';
///////////////////////////////////////////////////////////////////////////////